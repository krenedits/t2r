import './App.css';
import MainPage from './Components/MainPage';
import { connect } from 'react-redux';
import WaitingRoom from './Components/WaitingRoom';
import Board from './Components/Board';
import ResultsPage from './Components/ResultsPage';
import { useEffect } from 'react';
import { socket } from './utils/API';
import serverActions from './store/server/actions'
import gameActions from './store/game/actions'
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import Messages from './Components/Messages/index';

function getContent(gameState) {
  switch (gameState) {
    case "MAIN_PAGE":
      return <MainPage/>;
    case "WAITING_FOR_PLAYERS":
      return <WaitingRoom/>;
    case "IN_GAME":
      return <Board/>
    case "RESULTS_PAGE":
      return <ResultsPage/>
    default:
      return <h1>404</h1>;
  }
}

function App(props) {
  const {gameState, roomId, setRoomState, notification} = props;

  useEffect(() => {
    if (gameState === "MAIN_PAGE")
      socket.on('state-changed', response => setRoomState(response));
    socket.on('action-sent', response => props.actionSent(response))
  },[setRoomState, roomId, gameState])

  return (
    <>
      {getContent(gameState)}
        {roomId && <Messages/>}
        <Snackbar
            anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}
            open={notification !== null} 
            autoHideDuration={2000}
            onClose={props.removeNotification}
        >
            <Alert severity={notification?.type || "info"}>
            {notification?.message || ""}
            </Alert>
        </Snackbar>
    </>
  );
}

function mapState(state) {
  const { gameState, roomId, notification } = state.server;
  return { gameState, roomId, notification };
}

const actionCreators = {
  setRoomState: serverActions.setRoomState,
  removeNotification: serverActions.removeNotification,
  actionSent: gameActions.actionSent
};

export default connect(mapState, actionCreators)(App);
