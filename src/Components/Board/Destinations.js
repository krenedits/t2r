import { makeStyles, Table, TableBody, TableCell, TableHead, TableRow } from "@material-ui/core";
import { Close, Done } from "@material-ui/icons";
import { Children, useEffect, useState } from "react";
import colors from "../../styles/colors";
import { SubmitButton } from "../Common";
import Menu from '@material-ui/core/Menu';
import { connect } from "react-redux";
import gameActions from '../../store/game/actions';
import { isConnected } from "../../utils/isConnected";

const useStyles = makeStyles(() => ({
    done: {
        color: "green",
        fontSize: "xx-large"
    },
    close: {
        color: "red",
        fontSize: "xx-large"
    },
    row: {
        '&:hover': {
            backgroundColor: colors.lightBrown,
            cursor: "pointer"
        }
    }
}));

function Destinations(props) {
    const {player} = props;
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);
    const cityTuples = player.connections.map(connection => [connection.fromCity, connection.toCity]);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
      };
    
    const handleClose = () => {
        setAnchorEl(null);
    };

    useEffect(() => {
        player.destinations.map((destination, i) => {
            const connected = isConnected(destination.fromCity, destination.toCity, cityTuples);
            if (connected.length > 0 && connected[0]) {
                let copy = player.destinations; 
                if (!copy[i].done || copy[i].done.includes(false) ) {
                    if (connected[0][0]) {
                        copy[i].done = connected[0][0].map(tuple => player.connections.find(connect => 
                            (connect.toCity === tuple[0] || connect.fromCity === tuple[0]) 
                            && (connect.toCity === tuple[1] || connect.fromCity === tuple[1]) 
                        ));
                        props.changeForm({...player, destinations: copy}, "player");
                    }
                }

            }

            return 0;
        } )
    }, [player, cityTuples, props])

    const handleHover = destination => {
        props.changeForm({
            from: destination.fromCity, 
            to: destination.toCity, 
            ids: destination.done?.length > 0 ? destination.done.map(route => route.id) : []
        }, "actives");
    }
    
    return(
        <>
            <SubmitButton onClick={handleClick}>
                Teljesítendő célok
            </SubmitButton>
            <Menu
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Honnan</TableCell>
                            <TableCell>Hová</TableCell>
                            <TableCell>Teljesítve</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            Children.toArray(player.destinations.map(destination =>
                                <TableRow 
                                    className={classes.row} 
                                    title="Cél mutatása"
                                    onMouseEnter={() => handleHover(destination)}
                                    onMouseLeave={() => props.changeForm({from: null, to: null}, "actives")}
                                >
                                    <TableCell>{destination.fromCity}</TableCell>
                                    <TableCell>{destination.toCity}</TableCell>
                                    <TableCell align="center">
                                    {
                                        destination.done ?
                                        <Done className={classes.done}/> : <Close className={classes.close}/>
                                    }
                                    </TableCell>
                                </TableRow>
                            ))
                        }
                    </TableBody>
                </Table>
            </Menu>
        </>
    );
}

function mapState(state) {
    const { cities, player, form } = state.game;
    return { cities, player, form };
}

const actionCreators = {
    changeForm: gameActions.changeForm
};

export default connect(mapState, actionCreators)(Destinations);