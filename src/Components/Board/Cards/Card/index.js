import { CardActionArea, CardContent, CardMedia, makeStyles, Typography } from '@material-ui/core';
import CardBasic from '@material-ui/core/Card';
import colors from '../../../../styles/colors';
import Train from '../../../../train.png';
import SuperTrain from '../../../../supertrain.png';
import Logo from '../../../../logo.png';
import Destination from '../../../../destination.jpg';
import './animation.css';
import { useState } from 'react';

const useStyles = makeStyles({
    root: {
        backgroundColor: colors.primaryHover,
        color: colors.white,
        height: "13%",
        width: "100%",
        transition: "1s all",
        '&:hover': {
            cursor: "pointer",
        },
    },
    inDeck: {
        transition: "1s height, 2s bottom, 4s left",
        height: 150,
        bottom: 0,
        '&:hover': {
            position: "absolute",
            height: 250,
            bottom: 0
        },
    },
    inDeckOut: {
        transition: "1s height, 2s bottom, 4s left",
        position: "absolute",
        bottom: 200,
        left: -1200,
    },
    media: {
        position: "relative",
        height: 70,
        width: "100%",
        backgroundSize: "contain",
    },
    maxHeight: {
        height: "100%",
        width: "100%"
    },
    rail: {
        position: "relative",
        width: "100%",
        height: "100%"
    },
  });

export default function Card(props) {
    const classes = useStyles();
    const { destination, inDeck, selected } = props;
    const [clicked, setClicked] = useState(false);

    return(
        <CardBasic 
            className={classes.root + " " + (inDeck && classes.inDeck)}
            onClick={props.onClick || (() => setClicked(!clicked))}
            style={{
                border: selected && "1px solid red",
                animationName: selected && "pick"
            }}
        >
            {!props.back ? 
            <CardActionArea className={classes.maxHeight}>
                { props.destination ?
                    <CardContent >
                        <Typography gutterBottom variant="h5" component="h2" align="center">
                            {props.fromCity ? props.fromCity : "BERLIN"}
                        </Typography>
                        <Typography gutterBottom variant="h5" component="h2" align="center">
                            -
                        </Typography>
                        <Typography gutterBottom variant="h5" component="h2" align="center">
                            {props.fromCity ? props.toCity : "AMSTERDAM"}
                        </Typography>
                    </CardContent>
                    :
                    <>
                    {!props.back && inDeck && <Typography align="center">{props.count || 3}</Typography>}
                    <CardMedia
                        className={classes.media}
                        image={props.back ? (destination ? Destination : Logo) : (props.color === "super" ? SuperTrain : Train)}
                        style={{
                            position: "relative",
                            height: "100%",
                            ...props.color !== "super" ? {
                                backgroundColor: props.color || "green"
                            } : {
                                animation: "pulse 20s infinite",
                                transition: "all 1s ease-out"
                            }
                        }}
                    />
                    </>
                }
            </CardActionArea>
            :
            <CardMedia
                className={classes.media}
                image={props.back ? (destination ? Destination : Logo) : Train}
                style={{
                    position: "relative",
                    height: "100%"
                }}
            />
            }
        </CardBasic>
    );
}