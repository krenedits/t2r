import { Grid, makeStyles } from "@material-ui/core";
import ReactCardFlip from "react-card-flip";
import { useState } from "react";
import Card from "./Card";

const useStyles = makeStyles(() => ({
    cardBack: {
        position: "relative",
        height: "200px",
        width: "auto",
    },
}));

export default function Cards(props) {
    const {destination, inDeck} = props;
    const classes = useStyles();
    const [isFlipped, setIsFlipped] = useState(false);
    
    return (
        <Grid 
            item
            xs={12}
            onClick={() => setIsFlipped(!isFlipped)}
        >
            <ReactCardFlip isFlipped={!inDeck && isFlipped} flipDirection="horizontal">
                <Grid item xs={12}>
                    <Card className={classes.cardBack} {...props}/>
                </Grid>
                <Grid item xs={12}>                
                    <Card className={classes.cardBack} back destination={destination ? true : false}/>
                </Grid>
            </ReactCardFlip>
        </Grid>
    );
}