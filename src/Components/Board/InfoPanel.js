import { Divider, Grid, makeStyles, Typography } from "@material-ui/core";
import { Explore, Train, ViewArray } from "@material-ui/icons";
import { Children } from "react";
import { useSelector } from "react-redux";
import ReactLoading from "react-loading";
import colors from "../../styles/colors";

const useStyles = makeStyles(theme => ({
    first: {
        marginTop: "5%"
    },
    news: {
        fontSize: "smaller"
    },
    scoreboard: {
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
        marginBottom: theme.spacing(1)
    },
    active: {
        backgroundColor: colors.primaryHover,
        color: colors.white,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        transition: "all 1s"
    },
    container: {
        transition: "all 1s",
        margin: theme.spacing(1)
    }
}));

export default function InfoPanel() {
    const classes = useStyles();
    const {player, players, history, index} = useSelector(state => state.game);
    return(
        <Grid item container xs={12} direction="column" style={{marginLeft: "5px"}} className={classes.container}>
            <Typography className={classes.first} variant="h6">Események</Typography>
                {
                    history?.length > 0 ?
                    Children.toArray(history.slice(-2).map(log =>
                        <>
                            <Typography className={classes.news}>{log}</Typography>
                            <Divider/>
                        </>
                    ))
                    :
                    <>
                        <Typography className={classes.news}><i>Nincs előzmény</i></Typography>
                        <Divider/>
                    </>
                }
            {
                Children.toArray(players.map(p => 
                            <div className={p.index === index ? classes.active : ""}>
                                <Typography  className={classes.first}>{p.name + (p.name === player.name ? " (Te)" : "" )} (Psz: {p.points})</Typography>
                                {
                                    p.index === index &&
                                    <ReactLoading type="cylon" color="#fff"/>
                                }
                                <Typography className={classes.scoreboard}>
                                    <Train/> {p.wagons} | <ViewArray/> {p.cards?.length || ""} | <Explore/> {p.destinations?.filter(dest => dest.done).length || 0}
                                </Typography>
                                <Divider/>
                            </div>
                ))
            }
        </Grid>
    );
}