import Card from './Cards/Card';
import gameActions from '../../store/game/actions';
import serverActions from '../../store/server/actions';
import { connect } from 'react-redux';
import { Children } from 'react';
import { shuffle } from '../../utils/shuffle';

function CardsOut(props) {
    const {player, cards, gamePhase, index, players } = props;

    const drawCard = color => {
        if ((gamePhase !== "DRAWING" && gamePhase !== "NEUTRAL") || player.index !== index) {
            if (player.index !== index)
                props.addNotification({
                    type: "warning", 
                    message: players[(players.findIndex(p => p.index === player.index) + 1) % players.length].name + " van most soron, nem húzhatsz kártyát!"
                })
            return;
        }
        if (gamePhase === "NEUTRAL")
            props.changeGamePhase("DRAWING");
        const colorObject = player.cards.filter(card => card.color === color)[0];
        const remaining = player.cards.filter(card => card.color !== color);
        !colorObject ? 
            remaining.push({color: color, count: 1}) : 
                remaining.push({color: color, count: colorObject.count + 1});

        props.changeForm({...player, cards: remaining}, "player");
        props.drawCard({color: color});
        if (player.drawn + (color === "super" ? 2 : 1) >= 2)
            props.changePlayer();
    }

    const handleDestinationClick = () => {
        if (gamePhase !== "NEUTRAL" || player.index !== index) {
            if (player.index !== index)
                props.addNotification({
                    type: "warning", 
                    message: players[(players.findIndex(p => p.index === player.index) + 1) % players.length].name + " van most soron, nem húzhatsz új célt!"
                })
            return;
        }
        else
            props.changeGamePhase("NEW_DESTINATION", player)
    }

    const threeSuperTrain = array => 
        array.slice(0,5).filter(card => card.color === "super").length >= 3 ? //háromnál vagy több SZUPERMOZDONY van kint?
        threeSuperTrain(shuffle(array)) //megkeverjük
        : Children.toArray(cards.slice(0,5).map(connect => <Card color={connect.color} onClick={() => drawCard(connect.color)} style={{transition: "all 2s"}}/>))
        
    return(
        <>
            <Card destination back onClick={handleDestinationClick} />
            {
                threeSuperTrain(cards)  
            }
            {
                cards.length > 5 &&
                <Card back onClick={() => drawCard(cards[5].color)}/>
            }
        </>
    );
}

function mapState(state) {
    const { cards, gamePhase, index, players } = state.game;
    return { cards, gamePhase, index, players };
}

const actionCreators = {
    drawCard: gameActions.drawCard,
    changeGamePhase: gameActions.changeGamePhase,
    changePlayer: gameActions.changePlayer,
    addNotification: serverActions.addNotification
};

export default connect(mapState, actionCreators)(CardsOut);