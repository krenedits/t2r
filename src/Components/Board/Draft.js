import { Grid, makeStyles } from "@material-ui/core"
import { Children } from "react";
import { SubmitButton } from "../Common";
import Card from "./Cards/Card";
import gameActions from '../../store/game/actions';
import { connect } from "react-redux";
import { shuffle } from "../../utils/shuffle";

const useStyles = makeStyles(() => ({
    pickingStage: { 
        position:"absolute",
        width: "200vw",
        height: "200vh",
        zIndex: 1000,
        background: "rgb(0,0,0,0.75)",
        transition: "2s height, 2s opacity, 2s background",
        opacity: 1
    },
    pickingStageOff: {
        transition: "2s height, 2s opacity, 2s background",
        position:"absolute",
        opacity: 0,
        height: 0,
        zIndex: 1000,
        background: "rgb(0,0,0,0)",
    },
    pickingStageCard: {
        margin: 20,
        transition: "2s all",
        height: "100%"
    },
    pickingStageCardOff: {
        margin: -200,
        height: 0,
        transition: "2s all"
    },
    pickingStageButton: {
        position: "absolute", 
        top: "20%",
        transition: "2s all"
    },
    pickingStageButtonOff: {
        display: "none",
        transition: "2s all"
    },
}));

function Draft(props) {
    const { player, gamePhase, destinations } = props;
    const handleDestinationSelect = destination => {
        player.destinations.map(dest => dest.id).includes(destination.id) ?
            props.changeForm({...player, destinations: player.destinations.filter(dest => dest.id !== destination.id)}, "player")
                : props.changeForm({...player, destinations: [...player.destinations, destination]}, "player")
    }

    const handleClick = () => {
        props.changeForm(shuffle(destinations.filter(dest => !player.destinations.map(d => d.id).includes(dest.id))), "destinations"); //kivéve azt a lapot megkeveri
        props.changePlayer();
    }
    
    const classes = useStyles();
    return(
        <Grid item container xs={12} justify="center" className={gamePhase === "NEW_DESTINATION" ? classes.pickingStage : classes.pickingStageOff}>
            <SubmitButton 
                onClick={handleClick} 
                className={gamePhase === "NEW_DESTINATION" ? classes.pickingStageButton : classes.pickingStageButtonOff}
                disabled={player.destinations.length < 1}
            >
                Mehet
            </SubmitButton>
            {
                Children.toArray(destinations.slice(0,3).map(destination => 
                    <Grid 
                        item xs={3}
                        className={gamePhase === "NEW_DESTINATION" ? classes.pickingStageCard : classes.pickingStageCardOff}
                        onClick={() => handleDestinationSelect(destination)}
                    >
                        <Card
                            destination 
                            fromCity={destination.fromCity} 
                            toCity={destination.toCity}
                            selected={player.destinations.filter(dest => dest.id === destination.id).length > 0}
                        />
                    </Grid>
                ))
            }
        </Grid>
    );
}

function mapState(state) {
    const { player, gamePhase, destinations } = state.game;
    return { player, gamePhase, destinations };
}

const actionCreators = {
    changeForm: gameActions.changeForm,
    changePlayer: gameActions.changePlayer,
    
};

export default connect(mapState, actionCreators)(Draft);