import { Grid, makeStyles, Typography } from "@material-ui/core";
import Map from '../../bigmappng.png';
import {ticketToRideData} from '../../map/ticket-to-ride-data';
import { City, Connection } from "../Common";
import { Children, useEffect, useState } from "react";
import Cards from "./Cards";
import { connect, useSelector } from "react-redux";
import gameActions from '../../store/game/actions';
import serverActions from '../../store/server/actions';
import colors from "../../styles/colors";
import { Train } from "@material-ui/icons";
import Destinations from "./Destinations";
import Draft from "./Draft";
import CardsOut from "./CardsOut";
import InfoPanel from "./InfoPanel";
import { socket } from "../../utils/API";

const useStyles = makeStyles(() => ({
    map: {
        position: "relative",
        backgroundColor: colors.primary,
        borderRadius: 30
    },
    img: {
        position: "relative",
        width: "100%",
    },
    cardBack: {
        position: "relative",
        height: "200px",
        width: "auto",
        
    },
    side: {
        backgroundColor: colors.lightBrown,
        transition: "all 1s",
    },
    scoreboard: {
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
}));

function Board(props) {
    const classes = useStyles();
    const { player, form, index, players, initializeGame, playersInRoom, gameState } = props;
    const [connectionActives, setConnectionActives] = useState([]);
    const { roomId } = useSelector(state => state.server);

    useEffect(() => { 
        socket.off('state-changed');
        socket.on('state-changed', response => props.stateChanged(response.state));
    }, [])

    useEffect(() => {
        initializeGame(roomId, form.name, playersInRoom || []);
    }, [initializeGame, playersInRoom])

    useEffect(() => {
        if (gameState === "RESULTS_PAGE")
            props.changeGameState("RESULTS_PAGE");
    },[gameState])

    useEffect(() => {
        if (player?.destinations.length === 0 && index === player.index)
            props.changeGamePhase("NEW_DESTINATION")

        const currentIndex = players.findIndex(p => p.name === player.name);
        if (form.numberOfPlayers > 2) {
            if (currentIndex > -1 && players[(currentIndex + 1) % players.length].wagons <= 2) {
                props.changeGameState("RESULTS_PAGE")
            }
        }
        else {
            if (currentIndex > -1 && player.wagons <= 2) {
                props.changeGameState("RESULTS_PAGE")
            }
        }
    }, [player, form.numberOfPlayers, players, props])

    return(
        <Grid item container justify="space-around"  xs={12} className={classes.side} spacing={1}>
            { player &&
            <>
            <Draft />
            <Grid item container xs={2} className={classes.side}>
                <InfoPanel numberOfPlayers={form.numberOfPlayers} changeGameState={props.changeGameState}/>
            </Grid>
            <Grid item container xs={8} className={classes.map}>
                <img src={Map} className={classes.img} alt="map"/>
                {Children.toArray(Object.values(ticketToRideData.cities).map((city, index) => 
                    <City 
                        city={city} 
                        active={connectionActives[index]?.active}
                        onChange={value => {
                            let tmp = [...connectionActives];
                            tmp[index].active = value;
                            
                            setConnectionActives(tmp);
                        }}
                    />
                ))} 
                {Children.toArray(Object.values(ticketToRideData.connections).map(connection => 
                        <div>
                        {
                            Children.toArray(connection.elements.map((element, i) =>
                                <Connection 
                                    connection={element} 
                                    color={connection.color} 
                                    group={connection.elements}
                                    from={connection.fromCity}
                                    to={connection.toCity}
                                    i={i}
                                    id={connection.id}
                                />
                            ))
                        }
                        </div>
                ))}
            </Grid>
            <Grid item container xs={2} justify="center" alignItems="center" >
                <CardsOut changeForm={props.changeForm} player={player}/>
            </Grid>
            <Grid item container xs={2} justify="flex-start" alignItems="flex-start" className={classes.side}>
                <Destinations player={player} connectionActives={connectionActives}/>
            </Grid>
            <Grid item container xs={6} justify="center" spacing={1} alignItems="center" className={classes.side}>
                {
                    Children.toArray(player?.cards.map(card => {
                        if (card.count > 0)
                            return (
                                <Grid item xs>
                                    <Cards inDeck={true} color={card.color} count={card.count}/>
                                </Grid>
                            )
                        return ""
                        }
                    ))
                }
            </Grid>
            <Grid item container xs={3} justify="center">
                <Grid item xs={6}>
                    <Typography variant="h6" className={classes.scoreboard}><Train/> {player?.wagons || 0}</Typography>
                </Grid>
                <Grid item xs={6}>
                <Typography variant="h6" className={classes.scoreboard}>Pontszám: {player?.points || 0}</Typography>
                </Grid>
            </Grid>
            </>
            }
        </Grid>
    );
}

function mapState(state) {
    const { cities, player, form, gamePhase, index, players, playersInRoom, gameState } = state.game;
    return { cities, player, form, gamePhase, index, players, playersInRoom, gameState };
}

const actionCreators = {
    changeForm: gameActions.changeForm,
    changeGameState: serverActions.changeGameState,
    changeGamePhase: gameActions.changeGamePhase,
    initializeGame: gameActions.initializeGame,
    stateChanged: gameActions.stateChanged
};

export default connect(mapState, actionCreators)(Board);