import { Grid } from '@material-ui/core';
import { Children } from 'react';
import { connect } from 'react-redux';
import { Dialog, SubmitButton, TextField } from '../Common/index';
import gameActions from './../../store/game/actions';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import colors from '../../styles/colors';

const useStyles = makeStyles(theme => ({
    textBubble: {
        padding: theme.spacing(2),
        borderRadius: 25,
        margin: theme.spacing(1)
    },
    you: {
        backgroundColor: colors.primary,
        color: colors.white
    },
    other: {
        backgroundColor: colors.backgroundLight,
    }
}));

function Messages(props) {
    const { messages, textbox, roomId, form } = props;
    const classes = useStyles();
    
    const handleSubmit = e => {
        e.preventDefault();
        props.sendMessage(textbox, roomId);
        props.changeForm(null, "textbox");
    }

    return (
        <Dialog title="Csevegés" opener={<SubmitButton>Csevegés</SubmitButton>} fullScreen action={<form onSubmit={handleSubmit}>
        <TextField
          onChange={e => props.changeForm(e.target.value, "textbox")}
          value={textbox || ""}
          autoFocus
        />
    </form>}>
            <Grid container>
            {
                Children.toArray(messages.map( message =>
                    <Grid item container xs={12} justify={message.person === form.name ? "flex-end" : "flex-start"}>
                        <Grid item className={classes.textBubble + " " + (message.person === form.name ? classes.you : classes.other)}>
                            <Typography>{message.person}: {message.msg}</Typography>
                        </Grid>
                    </Grid>
                ))
            }
          
          </Grid>
        </Dialog>
    );
}

function mapState(state) {
  const { messages, textbox, form } = state.game;
  const { roomId } = state.server;
  return { messages, textbox, roomId, form };
}

const actionCreators = {
  changeForm: gameActions.changeForm,
  sendMessage: gameActions.sendMessage
};

export default connect(mapState, actionCreators)(Messages);