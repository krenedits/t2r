import { Grid, makeStyles, Typography } from "@material-ui/core";
import { Dialog, Form, Select, SubmitButton, TextField, PageTemplate } from "../Common";
import gameActions from '../../store/game/actions';
import serverActions from '../../store/server/actions';
import { connect } from "react-redux";
import { useEffect } from "react";

const useStyles = makeStyles((theme) => ({
    sectionTitle: {
        marginBottom: theme.spacing(2)
    },
    section: {
        marginBottom: theme.spacing(3)
    }
}));

function MainPage (props) {
    const { form } = props;
    const classes = useStyles();
    const buttonProps = {
        fullWidth: true
    };

    const playerOptions = [
        {value: 2, label: 2},
        {value: 3, label: 3},
        {value: 4, label: 4},
        {value: 5, label: 5},
    ];

    useEffect(() => {
        props.initGameReducer();
        props.initServerReducer();
    }, [])

    return (
        <PageTemplate>
            <Grid item xs={12}>
                <Dialog
                    title="Új játék indítása"
                    opener={
                        <SubmitButton 
                            className={classes.submit} 
                            {...buttonProps}
                        >
                            Új játék indítása
                        </SubmitButton>
                    }
                    submittext="Létrehozás"
                    onSubmit={() => props.createRoom(form.numberOfPlayers, form.name)}
                >
                    <Typography>
                        Kalandra fel!
                    </Typography>
                    <Form
                        changeform={props.changeForm}
                        form={form}
                        name="form"
                    >
                        <Select
                            selectLabel="Játékosok száma"
                            optionList={playerOptions}
                            value={form.numberOfPlayers || ""}
                            name="numberOfPlayers"
                            format={{ xs: 12 }}
                        />
                        <TextField
                            name="name"
                            label="Játékosnév"
                            value={form.name || ""}
                            format={{ xs: 12 }}
                            disabled={form.ready}
                        />
                    </Form>
                </Dialog>
            </Grid>
            <Grid item xs={12}>
                <Dialog
                    title="Csatlakozás meglévő szobához"
                    opener={
                        <SubmitButton {...buttonProps}>
                            Csatlakozás meglévő szobához
                        </SubmitButton>
                    }
                    submittext="Csatlakozás"
                    onSubmit={() => props.joinRoom(form.roomId, form.name)}
                >
                    <Typography>
                        Kalandra fel!
                    </Typography>
                    <Form
                        changeform={props.changeForm}
                        form={form}
                        name="form"
                    >
                        <TextField
                            label="Szoba száma"
                            value={form.roomId || ""}
                            name="roomId"
                            format={{ xs: 12 }}
                        />
                        <TextField
                            name="name"
                            label="Játékosnév"
                            value={form.name || ""}
                            format={{ xs: 12 }}
                            disabled={form.ready}
                        />
                    </Form>
                </Dialog>
            </Grid>
            <Grid item xs={12}>
                <Dialog
                    title="Szabályok"
                    opener={
                        <SubmitButton {...buttonProps}>
                            Szabályok
                        </SubmitButton>
                    }
                >
                    <Typography variant="h5" className={classes.sectionTitle}>
                        A játék kezdete
                    </Typography>
                    <Typography className={classes.section}>
                        Mielőtt az első kör elkezdődne, a játékosoknak ki kell választaniuk, hogy melyik menetjegyeket tartják meg az eredetileg nekik osztottak
                        közül. Minimum kettőt kell megtartaniuk, bár tarthatnak meg többet is. A ki nem választott jegyeket tegyétek vissza a dobozba anélkül, hogy
                        bárki látná, hogy mi van rajtuk. A jegyek, amiket félretesztek, egyaránt lehetnek hosszú útvonalú és normál menetjegyek is. Azokat a
                        jegyeket, amelyek mellett döntöttek a játékosok, a játék végéig meg kell tartaniuk.
                    </Typography>
                    <Typography variant="h5" className={classes.sectionTitle}>
                        A játék célja
                    </Typography>
                    <Typography>
                    A játék célja a legtöbb pont megszerzése. Pontot a következőkkel lehet szerezni:
                    </Typography>
                    <ul>
                        <li> Két szomszédos város közötti útvonal megszerzésével;</li>
                        <li> Saját menetjegyeken szereplő 2 város közötti folyamatos útvonalsor sikeres
                        kiépítésével;</li>
                        <li> A leghosszabb folyamatos útvonalsor létrehozásával elnyerve az Európai
                        Expressz Bónusz kártyát;</li>
                        <li> És minden vasútállomásért, amit a játék végéig megmarad a játékos előtt.</li>
                    </ul>
                    <Typography>
                        <i> 
                            A játékos összpontjából le kell vonni minden olyan pontot, ami a nem teljesített
                            menetjegykártyákon található
                        </i>
                    </Typography>
                    
                </Dialog>
            </Grid>
        </PageTemplate>
    );
}

function mapState(state) {
    const { gameState, form } = state.game;
    return { gameState, form };
}

const actionCreators = {
    changeGameState: serverActions.changeGameState,
    changeForm: gameActions.changeForm,
    createRoom: serverActions.createRoom,
    joinRoom: serverActions.joinRoom,
    initGameReducer: gameActions.initReducer,
    initServerReducer: serverActions.initReducer,
};

export default connect(mapState, actionCreators)(MainPage);