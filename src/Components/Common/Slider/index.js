import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';

const useStyles = makeStyles({
  root: {
    width: 300,
  },
});

export default function DiscreteSlider(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
        <Typography gutterBottom>
            {props.title}
        </Typography>
        <Slider
            defaultValue={2}
            valueLabelDisplay="auto"
            step={1}
            marks
            min={2}
            max={5}
        />
    </div>
  );
}
