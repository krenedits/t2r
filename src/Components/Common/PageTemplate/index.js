import { Container, Grid, makeStyles, Typography, useMediaQuery, useTheme } from "@material-ui/core";
import Particles from "react-tsparticles";
import Logo from "../../../logo.png";
import colors from "../../../styles/colors";

const useStyles = makeStyles((theme) => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        
    },
    avatar: {
        margin: theme.spacing(3),
        position: "relative",
        height: 'auto',
        width: '40%',
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 0, 0),
    },
    title: {
        textAlign: "center",
        zIndex: 1
    },
    wrapper: {
        position: 'relative',
        padding: 'unset',
    },
    base: {
        padding: '10px',
        marginTop: theme.spacing(4)
    },
    containerMobile: {
        position: 'relative',
        display: "block",
        verticalAlign: "middle",
    },
}));

export default function PageTemplate (props) {
    const classes = useStyles();
    const theme = useTheme();
    const {noLogo} = props;

    const matches = useMediaQuery(theme.breakpoints.up('sm'));

    return (
        <>
            <Particles
        id="tsparticles"
        options={{
          background: {
            color: {
              value: "#FFE4C4",
            },
          },
          fpsLimit: 60,
          interactivity: {
            detectsOn: "canvas",
            events: {
              onClick: {
                enable: true,
                mode: "push",
              },
              onHover: {
                enable: true,
                mode: "repulse",
              },
              resize: true,
            },
            modes: {
              bubble: {
                distance: 40,
                duration: 500,
                opacity: 0.8,
                size: 40,
              },
              push: {
                quantity: 4,
              },
              repulse: {
                distance: 100,
                duration: 2.4,
              },
            },
          },
          particles: {
            color: {
              value: colors.primary
            },
            links: {
              color: colors.primary,
              distance: 150,
              enable: true,
              opacity: 0.5,
              width: 0.5,
            },
            collisions: {
              enable: true,
            },
            move: {
              direction: "none",
              enable: true,
              outMode: "bounce",
              random: true,
              speed: 2,
              straight: false,
            },
            number: {
              density: {
                enable: true,
                value_area: 800,
              },
              value: 100,
            },
            opacity: {
              value: 0.5,
            },
            shape: {
              type: "circle",
            },
            size: {
              random: false,
              value: 5,
            },
          },
          detectRetina: true,
        }}
        style={{
            position: "absolute",
            height: "100vh",
            width: "100%",
        }}
      />
        <Container component="main" maxWidth="xs" className={matches ? "" : classes.containerMobile}>
            
            <div className={matches ? classes.base : ""}>
                <Grid container spacing={3} className={classes.paper}>
                    {
                      !noLogo &&
                      <>
                      <img className={classes.avatar} src={Logo} alt={`Logo`}/>
                      <Typography component="h1" variant="h3" className={classes.title}>
                          Ticket To Ride
                      </Typography>
                      </>
                    } 
                    <Grid item container spacing={2} xs={12}>
                        {props.children}
                    </Grid>
                </Grid>
            </div>
        </Container>
        </>
    );
}