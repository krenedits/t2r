import { DialogActions, DialogContent, DialogTitle, IconButton, makeStyles, Slide } from '@material-ui/core';
import DialogBasic from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import { cloneElement, forwardRef, useState } from 'react';
import colors from '../../../styles/colors';
import SubmitButton from '../SubmitButton';
import { Grid } from '@material-ui/core';

const useStyles = makeStyles(() => ({
    title: {
        backgroundColor: colors.primary,
        color: colors.white,
    },
    dialog: {
        minWidth: "500px",
        minHeight: "500px", 
    },
    content: {
        padding: 0,
        "&:last-child": {
            paddingBottom: 0
        }
    }
}));

const Transition = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function Dialog(props) {
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    
    return(
        <>
            {cloneElement(props.opener, {
                onClick: handleOpen
            })}
            <DialogBasic
                open={open}
                onClose={handleClose}
                fullScreen={false}
                keepMounted
                TransitionComponent={Transition}
                {...props}
            >
                <DialogTitle className={classes.title}>
                    <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                        <CloseIcon/>
                    </IconButton>
                    {props.title}
                </DialogTitle>
                <DialogContent>
                    {props.children}
                </DialogContent>
                <DialogActions>
                    <Grid container justify="space-between">
                    {props.action}
                    <SubmitButton onClick={props.onSubmit || handleClose}>
                        {props.submittext || "Bezár"}
                    </SubmitButton>
                    </Grid>
                </DialogActions>
            </DialogBasic>
        </>
    );
}