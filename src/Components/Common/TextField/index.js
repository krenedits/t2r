import {withStyles} from "@material-ui/core/styles";
import colors from "../../../styles/colors.js";
import TxtField from "@material-ui/core/TextField";

const CssTextField = withStyles({
    root: {
        '& label.Mui-focused': {
            color: colors.primary,
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: colors.primary,
        },
        '& .MuiInput-underline:before': {
            borderBottomColor: colors.primary,
        },
        '& .MuiInput-underline:hover': {
            borderBottomColor: colors.primary,
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: colors.primary,
            },
            '&:hover fieldset': {
                borderColor: colors.primary,
            },
            '&.Mui-focused fieldset': {
                borderColor: colors.primary,
            },
        },
    },
})(TxtField);


const TextField = props => (
    <CssTextField 
        component="pre"
        margin={props.margin ? props.margin : "normal"}
        variant={props.variant ? props.variant : "outlined"}
        fullWidth={Object.prototype.hasOwnProperty.call(props, "fullWidth") ? props.fullWidth : true}
        {...props}
    />
)

export default TextField;