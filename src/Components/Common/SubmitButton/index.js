import {withStyles} from "@material-ui/core/styles";
import colors from "../../../styles/colors.js";
import { Button } from "@material-ui/core";

const StyledButton = withStyles({
    root: {
        background: colors.primary,
        borderRadius: 3,
        border: 0,
        color: colors.white,
        height: 50,
        padding:'0 20px',
        boxShadow: 'black',
        '&:hover': {
            background: colors.primaryHover,
            color: colors.white
        }
    },
    label: {
        textTransform: 'none',
        letterSpacing: "2px"
    },
})(Button);

export default StyledButton;