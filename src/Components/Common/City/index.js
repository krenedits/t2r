import { Tooltip } from "@material-ui/core";
import { useEffect, useState } from "react";
import { connect } from "react-redux";
import gameActions from '../../../store/game/actions';
import serverActions from '../../../store/server/actions';
import { ticketToRideData } from '../../../map/ticket-to-ride-data';

function City (props) {
    const { actives, player, city, gamePhase, players, index } = props;
    const [active, setActive] = useState(false);
    const [isBuilt, setIsBuilt] = useState(false);
    useEffect(() => setActive(actives.to === city.city || actives.from === city.city),[actives.to, actives.from, city.city])
    useEffect(() => 
        setIsBuilt(
            players.filter(player => player.connections.findIndex(connection => connection.fromCity === city.city || connection.toCity === city.city) > -1).length > 0
            || player.first === city.city
        )
    ,[player.connections, player.first, city.city, players])

    const buildValidator = () => {
        const connections = Object.values(ticketToRideData.connections);
        const index = connections.findIndex(connect => 
            (connect.toCity === city.city || connect.toCity === player.first) 
            && (connect.fromCity === city.city || connect.fromCity === player.first)
        );

        const superTrainCount = player.cards.find(c => c.color === "super")?.count || 0;
        
        return index > 0 && ((
            ((player.cards.filter(card => card.color === connections[index].color)[0]?.count || 0) + superTrainCount >= connections[index].elements.length)
            || (connections[index].color === "gray" && player.cards.filter(card => card.count + (card.color === "super" ? 0 : superTrainCount) >= connections[index].elements.length )))
            && (player.cards.find(c => c.color === "super")?.count || 0)) >= connections[index].locomotive
            ? connections[index] 
            : null;
    }

    const handleClick = () => {
        if ((gamePhase !== "NEUTRAL" && gamePhase !== "BUILDING") || index !== player.index) {
            if (index !== player.index) 
                props.addNotification({
                    message: players[(players.findIndex(p => p.index === player.index) + 1) % players.length].name + " van most soron, nem építhetsz!", 
                    type: "warning"
                })
            return;
        }
        if (player.first === null) {
            props.changeForm({...player, first: city.city}, "player")
            props.changeGamePhase("BUILDING")
        }
        else if (player.first !== null) {
            const result = buildValidator();
            if (result !== null) {
                props.buildConnection(result, player);
                props.changePlayer();
            }
            else {
                props.changeForm({...player, first: null}, "player");
                props.changeGamePhase("NEUTRAL")
            }
        }
    }

    return (
            <Tooltip title={city.city}>
                <div 
                    style={{ 
                        position: "absolute", 
                        left: `${city.x - (active ? 1 : 0.5)}%`, 
                        top: `${city.y - (active ? 1 : 0.5)}%`, 
                        paddingTop: active ? "2%" : "1%",
                        width: active ? "2%" : "1%",
                        borderRadius: "50%",
                        display: "inline",
                        transition: "all .5s",
                        backgroundColor: active ? "red" : (isBuilt ? "blue" : "white")
                    }}
                    onClick={handleClick}
                />
            </Tooltip>
    );
}

function mapState(state) {
    const { actives, player, form, gamePhase, players, index } = state.game;
    return { actives, player, form, gamePhase, players, index };
}

const actionCreators = {
    changeForm: gameActions.changeForm,
    buildConnection: gameActions.buildConnection,
    changeGamePhase: gameActions.changeGamePhase,
    changePlayer: gameActions.changePlayer,
    addNotification: serverActions.addNotification
};

export default connect(mapState, actionCreators)(City);