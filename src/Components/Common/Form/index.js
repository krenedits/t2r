import { Grid } from '@material-ui/core';
import React, { Children } from 'react';

/**
 * 
 * @param childComponent - Form-on belül való alkalmazáshoz. Ezzel egyszerűbb a formázás.
 */

export default function Form(props) {
    const { form } = props;

    const handleChange = e => {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        
        props.changeform({...form, [name]: value}, props.name)
    }

    const buildElem = child => (
        <Grid item {...child.props.format}>
            {
                React.cloneElement(child, 
                { 
                    onChange: handleChange,
                })
            }
        </Grid>
    )

    const contentGenerator = () => (
        <Grid item container spacing={1}>
            {Children.toArray(Children.toArray(props.children).map(child => {
                    if(!child.props.className?.includes("hidden"))
                        return buildElem(child)
                    else
                        return "";
                }
            ))}
        </Grid>
    )

    return props.childComponent ? 
        contentGenerator() :
        <form name={props.name} onSubmit={props.onSubmit}>
            {contentGenerator()}
        </form>
    
}