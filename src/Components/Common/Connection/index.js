import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {ticketToRideData} from '../../../map/ticket-to-ride-data';
import './animation.css';

export default function Connection(props) {
    const {connection, color, group, from, to, i, id} = props;
    const cities = [...Object.values(ticketToRideData.cities)];
    const fromCity = cities.filter(elem => elem.city === from)[0];
    const toCity = cities.filter(elem => elem.city === to)[0];
    const {player, actives, players} = useSelector(state => state.game)

    const [active, setActive] = useState(false);
    const [highlighted, setHighlighted] = useState(false);

    const isFirst = player?.first === to || player?.first === from;
    const isConnection = players.filter(player => player?.connections?.findIndex(connect => 
        (connect.toCity === to || connect.fromCity === to) && (connect.toCity === from || connect.fromCity === from)) > -1).length > 0

    useEffect(() => {
        const superTrainCount = player.cards.find(c => c.color === "super")?.count || 0;
        setActive((isFirst && 
            ((
                color === "gray" ?
                player.cards.filter(card => card.count + (card.color === "super" ? 0 : superTrainCount) >= group.length).length > 0
                :
                player.cards.find(card => card.color === color)?.count || 0) + (player.cards.find(card => card.color === "super")?.count || 0) >= group.length 
            )) 
            || isConnection)
    }
    ,[player?.first, player.connections, isConnection, isFirst, color, group.length, player.cards])

    useEffect(() => {
        setHighlighted(actives.ids?.includes(id))
    },[actives, id])

    const getDegree = (connection) => {
        let prev = {};
        let next = {};
        if (group.length === 1) {
            return Math.atan2(toCity.y - fromCity.y, toCity.x - fromCity.x) * 180 / Math.PI + 90;
        }
        if (i === 0) {
            prev.x = fromCity.x;
            prev.y = fromCity.y;
            next = group[i + 1];
        }
        else if (i === group.length - 1) {
            prev = group[i - 1];
            next.x = toCity.x;
            next.y = toCity.y;
        }
        else {
            prev = group[i - 1];
            next = group[i + 1];
        }

        const numerator = (connection.x - prev.x) * (next.x - prev.x) + (connection.y - prev.y) * (next.y - prev.y);
        const divider = Math.sqrt((prev.x - connection.x) ** 2 + (prev.y - connection.y) ** 2) * Math.sqrt((next.x - prev.x) ** 2 + (next.y - prev.y) ** 2);

        return (Math.acos(numerator / divider) + Math.atan2(next.y - prev.y, next.x - prev.x)) * 180 / Math.PI + 90;
    }

    return(
        <div 
            style={{ 
                position: "absolute", 
                left: `${active ? connection.x - 0.5 : 150}%`, 
                top: `${active ? connection.y - 2 : -100}%`, 
                height: "5%",  
                width: "1%",
                display: "inline",
                transition: "all 1s",
                backgroundColor: (active) && color,
                transform: highlighted ? `rotate(${getDegree(connection) + 360}deg)` : `rotate(${getDegree(connection)}deg)`,
                animationName: active && !isConnection && "flash",
                animationDuration: "1.5s",
                animationDelay: "0s",
                animationIterationCount: "infinite",
            }}
        />
    );
}