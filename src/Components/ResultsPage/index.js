import { Button, Grid, makeStyles, Typography, Table, TableBody, TableHead, TableRow, TableCell } from "@material-ui/core";
import { connect, useSelector } from "react-redux";
import { PageTemplate, SubmitButton, Dialog } from "../Common";
import serverActions from '../../store/server/actions';
import { Children, useEffect } from "react";
import { Done, Close } from "@material-ui/icons";
import { socket } from "../../utils/API";

const useStyles = makeStyles(theme => ({
    getFront: {
        zIndex: 1
    },
    backButton: {
        marginTop: theme.spacing(2),
        textAlign: "center"
    },
    done: {
        color: "green"
    },
    close: {
        color: "red"
    }
}));

function ResultsPage(props) {
    const classes = useStyles();
    const { players } = props;
    const { roomId } = useSelector(state => state.server);

    const getPoints = player => {
        let sum = player.points;
        player.destinations.map(dest => dest.done ? sum += dest.value : sum -= dest.value);
        return sum;
    }

    useEffect(() => socket.emit('sync-state', roomId,  {gameState: "RESULTS_PAGE"}, false, response => console.log(response.status)),[]);

    const handleBack = () => {
        props.leaveRoom(roomId);
        props.changeGameState("MAIN_PAGE");
    }

    const getContent = player => 
        <Table>
        <TableHead>
            <TableRow>
                <TableCell>Honnan</TableCell>
                <TableCell>Hová</TableCell>
                <TableCell>Teljesítve</TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
            {
                Children.toArray(player.destinations.map(destination =>
                    <TableRow style={{}}>
                        <TableCell>{destination.fromCity}</TableCell>
                        <TableCell>{destination.toCity}</TableCell>
                        <TableCell align="center">
                        {
                            destination.done ?
                            <Done className={classes.done}/> : <Close className={classes.close}/>
                        }
                        </TableCell>
                    </TableRow>
                ))
            }
        </TableBody>
    </Table>
    

    return(
        <PageTemplate>
            <Grid item container xs={12} className={classes.getFront} justify="center" spacing={3}>
                <Typography variant="h4" align="center">Végeredmény</Typography>
                {
                    Children.toArray(players.sort((a, b) => Number(getPoints(b)) - Number(getPoints(a))).map((player, i) =>
                        <Grid item>
                            <Typography variant="h5" align="center">
                                {i + 1}.: {player.name} ({getPoints(player)} pont)
                            </Typography>
                            <Dialog opener={<Button fullWidth>Részletek</Button>} title="Részletek">
                                {getContent(player)}
                            </Dialog>
                        </Grid>
                    ))
                }
                <Grid item className={classes.backButton}>
                    <SubmitButton fullWidth onClick={handleBack}>
                        Vissza a főmenübe
                    </SubmitButton>
                </Grid>
            </Grid>
        </PageTemplate>
    );
}

function mapState(state) {
    const { form, players } = state.game;
    return { form, players };
}

const actionCreators = {
    changeGameState: serverActions.changeGameState,
    leaveRoom: serverActions.leaveRoom
};

export default connect(mapState, actionCreators)(ResultsPage);