import { Avatar, Button, Grid, makeStyles, Tooltip } from "@material-ui/core";
import { Form, PageTemplate, SubmitButton, TextField } from "../Common";
import gameActions from '../../store/game/actions';
import serverActions from '../../store/server/actions';
import { connect, useSelector } from "react-redux";
import colors from "../../styles/colors";
import { Children, useEffect } from "react";
import { socket } from "../../utils/API";
import {CopyToClipboard} from 'react-copy-to-clipboard';
import { FileCopy } from "@material-ui/icons";

const useStyles = makeStyles(() => ({
    disabled: {
        backgroundColor: colors.disabledBg,
        color: colors.disabledFont
    },
    id: {
        textAlign: "center"
    },
    getFront: {
        zIndex: 1
    },
    icon: {
        backgroundColor: "green"
    },
    copy: {
        color: colors.primaryHover
    },
    roomId: {
        backgroundColor: colors.backgroundLight
    }
}));

function WaitingRoom(props) {
    const { form } = props;
    const classes = useStyles();
    const { roomId, roomState } = useSelector(state => state.server);

    useEffect(() => {
        socket.on('player-joined', response => props.playerJoined(response.socketId, response.roomId));
        socket.on('player-left', response => props.playerLeft(response.socketId));
        socket.on('room-is-full', response => {
            if (response.roomId === roomId) {
                props.changeGameState("IN_GAME");
            }
        })
    },[roomId, props]);
    
    return(
        <PageTemplate>
            <Grid item container xs={12} className={classes.getFront} spacing={3} alignContent="center">
                <Grid item container xs={12} justify="center" spacing={1}>
                    {
                        Children.toArray([...Array(roomState?.state?.roomSize || 0)].map((e, i) =>(
                            <Tooltip title={roomState?.state?.playersInRoom[i]?.name || "Nem elérhető"}>
                                <Grid item>
                                    <Avatar className={i < roomState?.state?.playersInRoom.length ? classes.icon : ""}/>
                                </Grid>
                            </Tooltip>
                        )))
                    }
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        value={roomId ? roomId : form.roomId}
                        multiline
                        className={classes.roomId}
                        label="Szoba száma"
                    />
                    <Grid item container justify="flex-end">
                            <CopyToClipboard 
                                text={roomId ? roomId : form.roomId} 
                                onCopy={() => props.addNotification({message: "Vágólapra másolva", type: "success"})}
                            >
                                <Button 
                                    className={classes.copy}
                                >
                                    <FileCopy/>
                                </Button>
                            </CopyToClipboard>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                <Form
                    form={form}
                    name="form"
                    changeform={props.changeForm}
                >
                    <SubmitButton
                        button="true"
                        format={{ xs: 12 }}
                        fullWidth
                        onClick={() => props.leaveRoom(roomId)}
                    >
                        Vissza
                    </SubmitButton>
                </Form>
                </Grid>
            </Grid>
        </PageTemplate>
    );
}

function mapState(state) {
    const { gameState, form } = state.game;
    return { gameState, form };
}

const actionCreators = {
    changeGameState: serverActions.changeGameState,
    changeForm: gameActions.changeForm,
    playerJoined: serverActions.playerJoined,
    playerLeft: serverActions.playerLeft,
    leaveRoom: serverActions.leaveRoom,
    closeRoom: serverActions.closeRoom,
    addNotification: serverActions.addNotification
};

export default connect(mapState, actionCreators)(WaitingRoom);