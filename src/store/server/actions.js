import { templateChangeForm, templateInitReducer } from '..';
import {socket} from '../../utils/API';
import { types } from './types';

const createRoom = (numberOfPlayers, name) => dispatch => {
    dispatch({type: types.CREATE_ROOM_REQUEST});
    socket.emit('create-room', 
        numberOfPlayers,
        response => {
            if (response.status === "ok") {
                const rState = {roomSize: numberOfPlayers, playersInRoom: [{socketId: socket.id, name: name}]};
                setRoomState(rState);
                syncState(dispatch, response.roomId, rState);
                dispatch({
                    type: types.CREATE_ROOM_SUCCESS, 
                    roomId: response.roomId, 
                    socketId: socket.id,
                    name: name
                }) 
            }
            else 
                dispatch({
                    type: types.CREATE_ROOM_FAILURE,
                    message: response.message
                })  
        }
    )
}

function changeGameState (value) {
    return dispatch => {
        dispatch({ type: types.CHANGE_GAME_STATE, value });
    }
}

const joinRoom = (roomId, name) => dispatch => {
    dispatch({type: types.JOIN_ROOM_REQUEST});
    socket.emit('join-room', 
        roomId,
        response => {
            if (response.status === "ok") {
                socket.emit('get-state', roomId, r => {
                    if (r.status === "ok") {
                        const rState = JSON.parse(r.state);
                        const changedState = {...rState, playersInRoom: [...rState.playersInRoom, {socketId: socket.id, name: name}]};
                        setRoomState(changedState);
                        syncState(dispatch, roomId, {...changedState, notification: {type: "info", message: name + " csatlakozott a szobához"}});
                        dispatch({type: types.JOIN_ROOM_SUCCESS, roomId: roomId, isFull: changedState.playersInRoom.length >= changedState.roomSize}) 
                    }
                    else
                        dispatch({type: types.JOIN_ROOM_FAILURE, message: r.message})
                })
            }
            else
                dispatch({type: types.JOIN_ROOM_FAILURE, message: response.message})
        }
    )
}

const syncState = (dispatch, roomId, state ) => {
    dispatch({type: types.SYNC_STATE_REQUEST});
    socket.emit('sync-state', 
        roomId,
        state,
        false,
        response => response.status === "ok" ?  
            dispatch({type: types.SYNC_STATE_SUCCESS}) 
            : dispatch({type: types.SYNC_STATE_FAILURE, message: response.message})
    )
}

const setRoomState = state => dispatch => dispatch({type: types.SET_ROOM_STATE, state: state});

const closeRoom = roomId => dispatch => {
    dispatch({type: types.CLOSE_ROOM_REQUEST});
    socket.emit('close-room', 
        roomId,
        response => response.status === "ok" ? 
            dispatch({type: types.CLOSE_ROOM_SUCCESS}) 
            : dispatch({type: types.CLOSE_ROOM_FAILURE, message: response.message})
    )
}

const leaveRoom = roomId => dispatch => {
    dispatch({type: types.LEAVE_ROOM_REQUEST});
    socket.emit('leave-room', 
        roomId,
        response => response.status === "ok" ? 
            dispatch({type: types.LEAVE_ROOM_SUCCESS}) 
            : dispatch({type: types.LEAVE_ROOM_FAILURE, message: response.message})
    )
}

const playerJoined = (socketId, roomId) => dispatch => {
    socket.emit('get-state', 
        roomId,
        response => response.status === "ok" ?
            dispatch({type: types.PLAYER_JOINED, socketId: socketId, name: JSON.parse(response.state)[socketId]})
            : dispatch({type: types.GET_STATE_FAILURE, message: response.message})
    )
}
const playerLeft = socketId => dispatch => dispatch({type: types.PLAYER_LEFT, socketId: socketId});  

const removeNotification = () => dispatch => dispatch({type: types.REMOVE_NOTIFICATION}); 

const addNotification = msg => dispatch => dispatch({type: types.ADD_NOTIFICATION, msg})

const actions = {
    createRoom,
    removeNotification,
    joinRoom,
    changeForm: templateChangeForm(types),
    initReducer: templateInitReducer(types),
    playerJoined,
    changeGameState,
    closeRoom,
    playerLeft,
    leaveRoom,
    setRoomState,
    addNotification
}

export default actions;