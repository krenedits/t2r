import { types } from "./types";

const initialState = {
    loading: false,
    roomId: null,
    gameState: "MAIN_PAGE",
    notification: null,
    roomState: null
}

export function server(state = initialState, action) {
    switch(action.type) {
        case types.INIT_REDUCER:
            return initialState;
        case types.CREATE_ROOM_REQUEST:
            return {
                ...state,
                loading: true
            } 
        case types.CREATE_ROOM_SUCCESS:
            return {
                ...state,
                roomId: action.roomId,
                gameState: "WAITING_FOR_PLAYERS"
            }
        case types.CREATE_ROOM_FAILURE:
            return {
                ...state,
                notification: {message: action.message, type: "error"}
            }
        case types.JOIN_ROOM_REQUEST:
            return {
                ...state,
                loading: true,
                error: null
            } 
        case types.JOIN_ROOM_SUCCESS:
            return {
                ...state,
                roomId: action.roomId,
                gameState: action.isFull ? "IN_GAME" : "WAITING_FOR_PLAYERS"
            }
        case types.JOIN_ROOM_FAILURE:
            return {
                ...state,
                notification: {message: action.message, type: "error"}
            }
        case types.PLAYER_JOINED:
            return {
                ...state,
            }
        case types.PLAYER_LEFT:
            return {
                ...state,
                roomState: {
                    ...state.roomState,
                    state: {
                        ...state.roomState.state,
                        playersInRoom: state.roomState.state.playersInRoom.filter(player => player.socketId !== action.socketId)
                    }
                }
            }
        case types.CHANGE_GAME_STATE:
            return {
                ...state,
                gameState: action.value,
            };
        case types.CLOSE_ROOM_REQUEST:
            return {
                ...state,
                loading: true,
                error: null
            } 
        case types.CLOSE_ROOM_SUCCESS:
            return {
                ...state,
                gameState: "IN_GAME"
            }
        case types.CLOSE_ROOM_FAILURE:
            return {
                ...state,
                notification: {message: action.message, type: "error"}
            }
        case types.LEAVE_ROOM_REQUEST:
            return {
                ...state,
                loading: true,
                error: null
            } 
        case types.LEAVE_ROOM_SUCCESS:
            return {
                ...state,
                gameState: "MAIN_PAGE",
                roomState: initialState.roomState
            }
        case types.LEAVE_ROOM_FAILURE:
            return {
                ...state,
                notification: {message: action.message, type: "error"}
            }
        case types.SYNC_STATE_REQUEST:
            return {
                ...state,
                loading: true,
                error: null
            } 
        case types.SYNC_STATE_SUCCESS:
            return {
                ...state
            }
        case types.SYNC_STATE_FAILURE:
            return {
                ...state,
                notification: {message: action.message, type: "error"}
            }
        case types.SET_ROOM_STATE:
            return {
                ...state,
                notification: action?.state?.state?.notification || null,
                roomState: {...state.roomState, ...action.state}
            }
        case types.ADD_NOTIFICATION:
            return {
                ...state,
                notification: action.msg
            }
        case types.REMOVE_NOTIFICATION:
            return {
                ...state,
                notification: null
            }
        default:
            return state;
    }
}