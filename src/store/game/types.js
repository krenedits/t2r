export const types = {
    CHANGE_GAME_STATE: "@@game/CHANGE_GAME_STATE",
    CHANGE_FORM: "@@game/CHANGE_FORM",
    CHANGE_PLAYER: "@@game/CHANGE_PLAYER",
    CHANGE_GAME_PHASE: "@@game/CHANGE_GAME_PHASE",
    DRAW_CARD: "@@game/DRAW_CARD",
    LOG_ACTION: "@@game/LOG_ACTION",
    BUILD_CONNECTION: "@@game/BUILD_CONNECTION",
    INITIALIZE_GAME: "@@game/INITIALIZE_GAME",
    CHANGE_NAME: "@@game/CHANGE_NAME",
    STATE_CHANGED: "@@game/STATE_CHANGED",

    SYNC_STATE_REQUEST: "@@game/SYNC_STATE_REQUEST",
    SYNC_STATE_SUCCESS: "@@game/SYNC_STATE_SUCCESS",
    SYNC_STATE_FAILURE: "@@game/SYNC_STATE_FAILURE",

    INIT_REDUCER: "@@game/INIT_REDUCER",

    SEND_MESSAGE: "@@server/SEND_MESSAGE",

    ACTION_SENT: "@@server/ACTION_SENT"
  };
  