import { types } from "./types";
import {ticketToRideData} from '../../map/ticket-to-ride-data.js';
import { socket } from "../../utils/API";

const initPlayer = {
  cards: [],
  destinations: [],
  wagons: 45,
  points: 0,
  name: "",
  connections: [],
  first: null,
  drawn: 0
}

const initialState = {
  gamePhase: "NEUTRAL",
  form: {
    numberOfPlayers: 2,
    roomId: null,
    name: "Vendég"
  },
  index: 0,
  cards: [], //felküldi
  destinations: [], //felküldi
  connections: ticketToRideData.connections,
  cities: ticketToRideData.cities,
  players: [], //felküldi
  history: [], //felküldi
  actives: { //felküldi
    to: null,
    from: null,
    ids: []
  },
  error: null,
  messages: [],
  textbox: null
};

const removeElement = (cards, element) => {
  const result = [];
  let i = 0;
  while (cards[i].color !== element.color) {
    result.push(cards[i]);
    i++;
  }

  return result.concat(cards.slice(i + 1));
}

const getPoints = length => {
  switch (length) {
    case 1:
      return 1;
    case 2:
      return 2;
    case 3:
      return 4;
    case 4:
      return 7;
    case 6:
      return 15;
    case 7:
      return 21;
    default:
      return 0;
  }
}

const syncState = (roomId, state) => {
  socket.emit('sync-state', 
      roomId,
      state,
      false,
      response => console.log(response.status)
  )
}

const syncAction = (roomId, action) => {
  socket.emit('sync-action', 
      roomId,
      action,
      false,
      response => console.log(response.status)
  )
}

export function game(state = initialState, action) {
  switch (action.type) {
    case types.INIT_REDUCER:
      return initialState;
    case types.CHANGE_PLAYER:
      syncState(state.roomId, {
        gamePhase: "NEUTRAL",
        players: state.players.map((elem, i) => i === state.player.index ? {...state.player, drawn: 0} : elem),
        index: (state.index + 1) % state.players.length,
      })
      return {
        ...state,
        gamePhase: "NEUTRAL",
        player: {
          ...state.player,
          drawn: 0
        },
        players: state.players.map((elem, i) => i === state.player.index ? state.player : elem),
        index: (state.index + 1) % state.players.length
      }
    case types.CHANGE_FORM:
      return {
        ...state,
        [action.data.name]: action.data.form,
      };
    case types.STATE_CHANGED:
      return {
        ...state,
        ...action.state
      }
    case types.ACTION_SENT:
      return {
        ...state,
        ...action.action.action
      }
    case types.DRAW_CARD:
      syncState(state.roomId, {
          cards: removeElement(state.cards, action.element),
          players: state.players.map(player => { 
            if (player.index === state.player.index)
              return ({...state.player, drawn: action.element === "super" ? 2 : state.player.drawn + 1})
            else 
              return player
          })
        }
      )
      return {
        ...state,
        cards: removeElement(state.cards, action.element),
        player: {
          ...state.player,
          drawn: action.element === "super" ? 2 : state.player.drawn + 1
        }
        
      } 
    case types.CHANGE_GAME_PHASE:
      return {
        ...state,
        player: action.data.player ? action.data.player : state.player,
        gamePhase: action.data.phase
      }
    case types.LOG_ACTION:
      syncState(state.roomId, {
          history: [
            ...state.history,
            state.player.name + ": " + action.action
          ]
        }
      )
      return {
        ...state,
        history: [
          ...state.history,
          state.player.name + ": " + action.action
        ]
      }
    case types.BUILD_CONNECTION:
      syncState(state.roomId, {
        gamePhase: "NEUTRAL",
        players: state.players.map((elem, i) => i === state.player.index ? 
          {
            ...state.player,
              first: null,
              cards: action.data.copyCards,
              connections: [
                ...state.player.connections,
                action.data.connection,
              ],
              wagons: state.player.wagons - action.data.connection.elements.length,
              points: state.player.points + getPoints(action.data.connection.elements.length)
          } 
          : elem),
        index: (state.index + 1) % state.players.length
      })
      return {
        ...state,
        player: {
          ...state.player,
            first: null,
            cards: action.data.copyCards,
            connections: [
              ...state.player.connections,
              action.data.connection,
            ],
            wagons: state.player.wagons - action.data.connection.elements.length,
            points: state.player.points + getPoints(action.data.connection.elements.length)
        }
      }
    case types.INITIALIZE_GAME:
      return {
        ...state,
        players: action.players,
        cards: action.cards,
        destinations: action.destinations,
        player: {
          ...initPlayer,
          name: action.name,
          index: action.players.findIndex(p => p.name === action.name)
        },
        roomId: action.roomId
      }
    case types.SYNC_STATE_REQUEST:
        return {
            ...state,
            loading: true,
            error: null
        } 
    case types.SYNC_STATE_SUCCESS:
        return {
            ...state
        }
    case types.SYNC_STATE_FAILURE:
        return {
            ...state,
        }
    case types.SEND_MESSAGE:
      syncAction(action.roomId, {
          messages: [...state.messages, {msg: action.msg, person: state.form.name}]
        }
      )
      return {
        ...state,
        messages: [...state.messages, {msg: action.msg, person: state.form.name}]
      }
    default:
      return state;
  }
}
