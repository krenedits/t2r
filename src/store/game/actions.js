import { templateChangeForm, templateInitReducer } from '..';
import { types } from './types';
import { messages } from '../../utils/historyMessages';
import { socket } from '../../utils/API';
import { cards } from '../../utils/cards.js';
import { shuffle } from "../../utils/shuffle";
import {ticketToRideData} from '../../map/ticket-to-ride-data.js';

const initPlayer = {
    cards: [],
    destinations: [],
    wagons: 45,
    points: 0,
    name: "",
    connections: [],
    first: null,
    drawn: 0
}

const changePlayer = () => dispatch => dispatch({type: types.CHANGE_PLAYER});

const drawCard = element => dispatch => {
    dispatch({type: types.DRAW_CARD, element});
    dispatch({type: types.LOG_ACTION, action: logAction("DRAW_CARD") });
}

const buildConnection = (connection, player) => dispatch => {
    let toPay = connection.elements.length;
    let copyCards = [...player.cards];
    const superIndex = copyCards.findIndex(card => card.color === "super");
    while (superIndex > -1 && copyCards[superIndex].count > 0 && toPay > 0) {
        copyCards[superIndex].count--;
        toPay--;
    }

    if (toPay > 0) {
        let sameColorIndex = connection.color === "gray" ? copyCards.findIndex(card => card.count >= toPay) : copyCards.findIndex(card => card.color === connection.color);
        if (sameColorIndex > -1)
            copyCards[sameColorIndex].count -= toPay;
        
    }
    const data = {
        connection,
        copyCards
    }
    dispatch({type: types.BUILD_CONNECTION, data});
    dispatch({type: types.LOG_ACTION, action: logAction("BUILD_CONNECTION", connection) });
}

const initialize = players => {
    let result = [];
    for (let i = 0; i < players.length; i++) {
      let copyPlayer = {};
      Object.assign(copyPlayer, initPlayer);
      
      copyPlayer.name = players[i].name;
      
      result.push(copyPlayer); 
    }
  
    return result;
  }

const changeGamePhase = (phase, player) => dispatch => {
    const data = {phase: phase, player: player ? player : null};
    dispatch({type: types.CHANGE_GAME_PHASE, data});
    syncState(dispatch, {gamePhase: phase});
}

const initializeGame = (roomId, name, players) => dispatch => {
    const dests = shuffle(Object.values(ticketToRideData.destinations));
    const pls = initialize(players);
    dispatch({ type: types.INITIALIZE_GAME, roomId, players: pls, name, cards, destinations: dests});
    syncState(dispatch, roomId, {players: pls, cards: cards});
}

const stateChanged = state => dispatch => dispatch({type: types.STATE_CHANGED, state});

const logAction = (action, options) => {
    switch(action) {
        case "DRAW_CARD":
            return messages.draw;
        case "BUILD_CONNECTION":
            return messages.build(options)
        case "DRAW_DESTINATION":
            return messages.destinationCardDrawing;
        default:
            return "ISMERETLEN MŰVELET";
    }
}

const syncState = (dispatch, roomId, state) => {
    dispatch({type: types.SYNC_STATE_REQUEST});
    socket.emit('sync-state', 
        roomId,
        state,
        false,
        response => response.status === "ok" ?  
            dispatch({type: types.SYNC_STATE_SUCCESS}) 
            : dispatch({type: types.SYNC_STATE_FAILURE, message: response.message})
    )
}

const sendMessage = (msg, roomId) => dispatch => dispatch({type: types.SEND_MESSAGE, msg, roomId});

const actionSent = action => dispatch => dispatch({type: types.ACTION_SENT, action});

const actions = {
    changeForm: templateChangeForm(types),
    initReducer: templateInitReducer(types),
    changePlayer,
    drawCard,
    changeGamePhase,
    buildConnection,
    logAction,
    initializeGame,
    stateChanged,
    syncState,
    sendMessage,
    actionSent
}

export default actions;