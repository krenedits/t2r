import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { createLogger } from "redux-logger";
import { game } from './game/reducer';
import { server } from './server/reducer';
import thunkMiddleware from "redux-thunk";

export const templateChangeForm = types => (form, name) => dispatch =>
  dispatch({ type: types.CHANGE_FORM, data: { form: form, name: name } });

export const templateInitReducer = types => () => dispatch => dispatch({ type: types.INIT_REDUCER });

const loggerMiddleware = createLogger();

const rootReducer = () => combineReducers({
    game,
    server
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
    rootReducer(),
    composeEnhancers(
        applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
        )
    )
);