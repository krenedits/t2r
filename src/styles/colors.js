const colors = {
    background: "#CD853F",
    primary: "#A0522D",
    primaryHover: "#8B4513",
    white: "#fff",
    disabledBg: "#e3d2bf",
    disabledFont: "#baac9c",
    lightBrown: "#DEB887",
    backgroundLight: "#FFE4C4"
};

export default colors;