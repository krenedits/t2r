const compare = (tupleOne, tupleTwo) => tupleOne[0] === tupleTwo[0] && tupleOne[1] === tupleTwo[1];

const returnOther = (base, tuple) => tuple[0] === base ? tuple[1] : tuple[0];

export const isConnected = (cityOne, cityTwo, connections, array) => {
    array = array === undefined ? connections : array;
    if (cityOne === cityTwo) {
        return array.filter(elem => connections.filter(connect => compare(connect, elem)).length === 0);
    }
    const containerTuples = connections.filter(tuple => tuple.includes(cityOne));

    if (containerTuples.length === 0)
        return false;

    return containerTuples.map(tuple => isConnected(returnOther(cityOne, tuple), cityTwo, connections.filter(cities => !compare(cities, tuple)), array));
    
}
