import { shuffle } from "./shuffle";

const colors = ["red", "purple", "blue", "black", "green", "yellow", "orange", "white"]

const cardsOfColors = [];

colors.map(color => {
    for (let i = 0; i < 12; i++) 
        cardsOfColors.push({color: color});
    return 0;
});

const cardsOfLocomotives = [];

for (let i = 0; i < 14; i++)
    cardsOfLocomotives.push({color: "super"});

export const cards = shuffle(cardsOfColors.concat(cardsOfLocomotives));