export const messages = {
    build: destination => `Kiépítette a ${destination.fromCity}-${destination.toCity} vonalat`,
    draw: "Vasútkocsi-kártyát húzott",
    done: goal => `Teljesítette a ${goal.fromCity}-${goal.toCity} vonalat`
}